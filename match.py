import re
from sets import Set

def makeMatchStrings(matchPositions, revMatchPositions, sequence):
    
    initialSeqList = ["0"] * len(sequence)
    
    matchSet, revMatchSet = Set(matchPositions), Set(revMatchPositions)
    matchOnly = list(matchSet - revMatchSet)
    revMatchOnly = list(revMatchSet - matchSet)
    both = list(matchSet.intersection(revMatchSet))
    
    for position in matchOnly: initialSeqList[position] = "1"
    
    for position in revMatchOnly: initialSeqList[position] = "2"

    for position in both: initialSeqList[position] = "3"
    
    return "".join(initialSeqList)


def findLtrPairs(sequenceString, spacerLength, patternLength, tolerance = 0):
    
    minSpacerLength, maxSpacerLength = spacerLength - tolerance, spacerLength + tolerance
    ltrPositions = []
    for position in range(0, len(sequenceString) - (2*patternLength + minSpacerLength) + 1):
        if (sequenceString[position] == "1") or (sequenceString[position] == "3"):
            if tolerance:
                for invPosition in range(position + patternLength + minSpacerLength, position + patternLength + maxSpacerLength + 1):
                    if (sequenceString[invPosition] == "2") or (sequenceString[invPosition] == "3"): ltrPositions.append((position, invPosition))
            else:
                invPosition = position + patternLength + spacerLength
                if (sequenceString[invPosition] == "2") or (sequenceString[invPosition] == "3"): ltrPositions.append((position, invPosition))
                
    return ltrPositions
    
    
    
