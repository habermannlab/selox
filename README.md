# SeLOX

SeLOX is a locus of recombination site search tool for the detection and 
directed evolution of site-specific recombination systems. It can identify 
degenerate lox-like sites within genomic sequences. SeLOX calculates a 
position weight matrix based on lox-like sequences, which is used to search 
genomic sequences. For computational efficiency, we transform sequences into 
binary space, which allows us to use a bit-wise AND Boolean operator for 
comparisons.

If you are using this tool, please cite: Surendranath V1, Chusainow J, Hauber J, 
Buchholz F, Habermann BH., (2010), SeLOX--a locus of recombination site search 
tool for the detection and directed evolution of site-specific recombination 
systems. Nucleic Acids Res. 2010 Jul;38(Web Server issue):W293-8. 
doi: 10.1093/nar/gkq523.

Main developer: Vineeth Surendranath (vineethsurendranath@gmail.com)


