from search import *
from match import *
from score import *
from Bio import Fasta

parser = Fasta.RecordParser()
iter = Fasta.Iterator(open("sequences.fa"), parser)
curr = iter.next()

sequence = curr.sequence

pHashList, rHashList, pMaxPW, rMaxPW = makePatternHashList(open("pattern.txt"))


mPos = searchPattern(pHashList, sequence, 2)
rmPos = searchPattern(rHashList, sequence, 2)

regex = makeRePattern(8,13,2)

matchString = makeMatchStrings(mPos, rmPos, sequence)
print matchString

ltrPos = findLtrPairs(matchString, 8, 13, 2)
print ltrPos

ltrSequences = getLTRs(curr.title, sequence, pHashList, rHashList)

for sequence in ltrSequences: print sequence
