from config import *
from search import *
from match import findLtrPairs, makeMatchStrings
complSymHash = {"A" : ["T"], "G" : ["C"], "T" : ["A"], "C" : ["G"], "N" : ["A","G","T","C"], "X": ["A","G","T","C"], "Y" : ["G","A"], "R" : ["T","C"], "W" : ["T","A"], "S" : ["C","G"], "K" : ["A","C"], "M" : ["G", "T"], "D" : ["T","C","A"], "V" : ["T","G","C"], "H" : ["T","G","A"], "B" : ["G","C","A"]}

def getLTRs(name, sequence, patternHashList, revPatternHashList):
    
    matches = searchPattern(patternHashList, sequence, maxNumMismatches)
    revmatches = searchPattern(revPatternHashList, sequence, maxNumMismatches)
 
    ltrPositions = findLtrPairs(makeMatchStrings(matches, revmatches, sequence), spacerLength, patternLength, spacerTolerance)
    
    ltrSequences = []
    
    for pos in range(0, len(ltrPositions)):
	
	ltrLeftStart = ltrPositions[pos][0]
	ltrRightStart = ltrPositions[pos][1]
	
	ltrLeft = sequence[ltrLeftStart:ltrLeftStart + patternLength]
	
	ltrSpacerStart = ltrLeftStart + patternLength
	ltrSpacerLength = ltrRightStart - ltrSpacerStart
	ltrSpacer = sequence[ltrSpacerStart:ltrSpacerStart + ltrSpacerLength]
	
	ltrRight = sequence[ltrRightStart:ltrRightStart + patternLength]
	
	ltrSequences.append((ltrLeft, ltrSpacer, ltrRight))
	
    return ltrSequences
	
    

def scoreLtrs(ltrSequences, patternHashList, revPatternHashList, maxPosWeight, revMaxPosWeight):
    
    ltrScores = {}
    ltrmatchStrings = {}
    for ltrSequence in ltrSequences:
	left, spacer, right = ltrSequence
	
	rightSequenceList = list(right)
	rightSequenceList.reverse()
	rightRev = "".join(rightSequenceList)
	
	
	score = 0
	asymmetry = 0
	leftpos, rightpos,asymmetrypos = [],[],[]
	for pos in range(0, patternLength):
	    patternHash, revPatternHash = patternHashList[pos], revPatternHashList[pos]
	    currNucl = left[pos]
	    currRevNucl = right[pos]
	    
	    
			
	    if patternHash.has_key(currNucl):
		score += patternHash[currNucl]
		leftMatch = True
		leftpos.append(" ")
	    else:
		score -= maxPosWeight[pos]
		leftMatch = False 
		leftpos.append(".")
		 
	    if revPatternHash.has_key(currRevNucl):
		score += revPatternHash[currRevNucl]
		rightMatch = True
		rightpos.append(" ")
		
	    else:
		score -= revMaxPosWeight[pos]
		rightMatch = False
		rightpos.append(".")

	    if rightRev[pos] not in complSymHash[currNucl]:
		asymmetrypos.append(".") 
		asymmetry += 1
	    else: asymmetrypos.append(" ")
	
	
	if asymmetry > maxAsymmetry: continue
	else:
	    if ltrScores.has_key(ltrSequence): ltrScores[ltrSequence].append(score)
	    else: ltrScores[ltrSequence] = [score]
            asymmetrypos.reverse()
            matchposString = "".join(leftpos) + " " + (" " * len(spacer)) + " " + "".join(rightpos)
	    asymmetryString = (" " * len(left)) + " " + (" " * len(spacer)) + " "  + "".join(asymmetrypos) 
            ltrmatchStrings[ltrSequence] = (matchposString, asymmetryString)        
	
    return (ltrScores, ltrmatchStrings)
	    
	    
	
	


