def writeConfigFile(timeStamp, spacer, spacerTolerance, asymmetry, mismatch):
    
    file = open("/tmp/ltr_%s/config.py" % timeStamp, "w")
    
    file.write("""patternLength = 13
spacerLength = %s
spacerTolerance = %s
maxAsymmetry = %s
maxNumMismatches = %s""" % (spacer, spacerTolerance, asymmetry, mismatch))
    
    file.close()
    
    return

def writeMainFile(timeStamp):
    
    file = open("/tmp/ltr_%s/main.py" % timeStamp, "w")
    
    mainimports = open("main.py").read()
    file.write(mainimports)
    
    file.write("from sys import path\npath.append('/tmp/ltr_%s')\n" % timeStamp)
    
    maincode=open("main_code.py").read()
    file.write(maincode)
    
    file.close()
    
    return
