from config import *
nucleotideToNumber = {"A" : 8, "G" : 4, "T" : 2, "C" : 1, "Y" : 3, "R" : 12, "W" : 10, "S" : 5, "K" : 6, "M" : 9, "D" : 14, "V" : 13, "H" : 11, "B" : 7, "X" : 15, "N" :  15}
complHash = {"A" : "T", "G" : "C", "C" : "G", "T" : "A", "Y" : "R", "R" : "Y", "W" : "W", "S" : "S", "K" : "M", "M" : "K", "D" : "H", "V" : "B", "B" : "V", "H" : "D", "X" : "X", "N" : "N"}

def makePatternHashList(patternHashFile):
    patternList = []
    revPatternList = []
    maxPositionWeight = []
    for line in patternHashFile:
        parts = line.strip().split(",")
        currNuclHash = {}
        currRevHash = {}
        positionWeights = []
        for part in parts:
            data = part.split(":")
            currNuclHash[data[0]] = float(data[1])
            currRevHash[complHash[data[0]]] = float(data[1])
            positionWeights.append(float(data[1]))
        
        positionWeights.sort()
        maxPositionWeight.append(positionWeights[-1])
        patternList.append(currNuclHash)
        revPatternList.append(currRevHash)
        
    revPatternList.reverse()
    revmaxPositionWeight = [item for item in maxPositionWeight]
    revmaxPositionWeight.reverse()

    return (patternList, revPatternList, maxPositionWeight, revmaxPositionWeight)




def patternHashListToBinary(patternHashList):
    
    patternBinary = []
    for nucleotideHash in patternHashList:
        currNucleotides = 0
        for nucleotide in nucleotideHash.keys(): currNucleotides += nucleotideToNumber[nucleotide]
        patternBinary.append(currNucleotides)
        
    return patternBinary


def sequenceToBinary(sequence): return map(lambda nucleotide: nucleotideToNumber[nucleotide], sequence)

def doBinaryAnd(searchList, targetList): return map(lambda searchBinary, targetBinary: searchBinary & targetBinary, searchList, targetList)

def isMatch(binaryAndResult, allowedNumMismatches): return binaryAndResult.count(0) <= allowedNumMismatches

def searchPattern(patternHashList, sequence, matchThreshold):
    
    matchPositions = []
    patternBinary = patternHashListToBinary(patternHashList)
    sequenceBinary = sequenceToBinary(sequence)
    
    for pos in range(0, len(sequence) - patternLength + 1):
        currSequenceBinary = sequenceBinary[pos:pos+patternLength]
        if isMatch(doBinaryAnd(patternBinary, currSequenceBinary), matchThreshold): matchPositions.append(pos)
    
       
    return matchPositions


    
    
    



