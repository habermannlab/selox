
nucleotideOrder = ["A", "G", "T", "C"]

def calPosWt(sequenceLines):
    
    posWts = {"A":[], "G":[], "T":[], "C":[]}
    left = []
    for line in sequenceLines:
        cleanLine = line.strip()
        if len(cleanLine): left.append(cleanLine[0:13])
        
    numberOfSequences = float(len(left))
    
    for pos in range(0,13):
        currPosHash = {}
        for sequence in left:
            nucleotide = sequence[pos]
            if currPosHash.has_key(nucleotide): currPosHash[nucleotide] += 1
            else: currPosHash[nucleotide] = 1
        for nucleotide in posWts.keys():
            if currPosHash.has_key(nucleotide): posWts[nucleotide].append(currPosHash[nucleotide]/numberOfSequences)
            else: posWts[nucleotide].append(0)
            
    
    return posWts
            
            
            
            
    
    
    
        
