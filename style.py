css="""

#searchBox {
   position: relative;
   top: 200px;
   left: 400px;
}

#workflow {
   position: relative;
   top: 150px;
   left: 200px;
}

p {
    font-family: HelveticaNeue;
    font-size: 18px;
    
}

#title{
    background-image: url('http://cluster-12.mpi-cbg.de/mpi-cbg.png');
    background-repeat: no-repeat;
    background-position: right;

    font-family: HelveticaNeue-CondensedBold;
    color: #0082DE;
    font-size: 96px;
    margin-bottom: auto;

}


#subtitle{
    font-family: HelveticaNeue-CondensedBold;
    color: #A8A8A8;
    font-size: 24px;
    margin-top: -20px;
    margin-left: -20px;
    
}

#grouptitle{
    font-family: HelveticaNeue-CondensedBold;
    font-size: 24px;
    color: #333333;
}

#info{
    font-family: HelveticaNeue-CondensedBold;
    font-seize: 16px;
    color: #A8A8A8;
    margin-top: -4px;
}

#sidemenu{
    font-family: HelveticaNeue-CondensedBold;
    font-seize: 16px;
    color: #333333;
}

#seqName{
    font-family: HelveticaNeue;
    font-size: 18px;
    color: #939393;
}

#sequence{
    font-family: HelveticaNeue-CondensedBold;
    font-size: 22px;
    color: #A8A8A8;
    
    
}

#ltrSeq{
    font-family: CourierNewPS-BoldMT,Courier-Bold,Courier;
    font-size: 24px;
    color: #333333;
    
}

#score{
    font-family: HelveticaNeue-CondensedBold;
    font-size: 24px;
    color: #B75B26
}

#pattern{
    font-family: HelveticaNeue-CondensedBold;
    color: #333333;
}

a:link{
    text-decoration: none;
}

a:visited{
    text-decoration: none;
}

a:hover{
    text-decoration: none;
}

"""
