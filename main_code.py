from sys import argv
#from Bio import Fasta
from Bio import SeqIO
from search import makePatternHashList
from score import getLTRs, scoreLtrs
from elchange import cleanEndings
import os
from pickle import dump


#print "GO"
patternHashFile = open(argv[1])
cleanEndings(argv[2])
sequenceFile = open(argv[2])
complHash = {"A" : "T", "G" : "C", "T" : "A", "C" : "G"}


patternHashList, revPatternHashList, maxPosWeights, revMaxPosWeights = makePatternHashList(patternHashFile)

#parser = Fasta.RecordParser()
#iter = Fasta.Iterator(sequenceFile, parser)
iter = SeqIO.parse(sequenceFile, "fasta")

scoreHash = {}
posStrings = {}
for curr in iter:
    #name, sequence = curr.title, curr.sequence.upper()
    name, sequence = curr.id, str(curr.seq.upper())

    #print "Processing :", name
    ltrs = getLTRs(name, sequence, patternHashList, revPatternHashList)
    #print ltrs
    
    scoreoutput = scoreLtrs(ltrs, patternHashList, revPatternHashList, maxPosWeights, revMaxPosWeights)
    scores = scoreoutput[0]
    matchpositions = scoreoutput[1]
    for key in scores.keys():
        ltr = "%s %s %s" % key
        for score in scores[key]:
            if scoreHash.has_key(score): scoreHash[score].append((name, ltr))
            else: scoreHash[score] = [(name, ltr)] 
    for key in matchpositions.keys():
        if not posStrings.has_key(key): posStrings[key] = matchpositions[key]

   
scores = scoreHash.keys()
scores.sort()
scores.reverse()

for score in scores:
    for ltr in scoreHash[score]:
        print "%s\t%s\t%s" % (ltr[0], ltr[1], score)

posStringFile = open(os.path.join(argv[3], "posstrings.txt"), "w")
for key in posStrings.keys():
    ltr = "%s %s %s" % key
    posStringFile.write("%s\t%s\t%s\n" % (ltr, posStrings[key][0], posStrings[key][1]))
posStringFile.close()
