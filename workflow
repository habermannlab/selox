#!/usr/bin/python2
from style import css

print "Content-type: text/html\n\n"
print "<html>"
print "<head>"
print "<style type='text/css'>"
print css
print "</style>"
print "</head>"
print "<body>"

print "<a style='text-decoration:none' href='http://selox.mpi-cbg.de/cgi-bin/selox/index'>"
print "<div id = 'title'>"
print "SeLOX"
print "<span id='subtitle'>A search tool for recombinase lox-like target sites</span>"
print "</div>"
print "</a>"

print "<div><div style='background: url(http://selox.mpi-cbg.de/selox/hivgenome.png) repeat-x; height: 54px; margin-left:-20px'/></div>"
print "<div>"
print "<div style='float:left;'><span id='grouptitle'>Workflow</span></div>"
print "<div style='float:right;'>"
print "<div style='width:100px;height:10px;background-color:#2682D5;text-align:center;padding:10px;'><span id='sidemenu'><a href='http://selox.mpi-cbg.de/cgi-bin/selox/index'>Home</a></span></div>"
print "<div style='margin-top:5px;width:100px;height:10px;background-color:#2682D5;text-align:center;padding:10px;'><span id='sidemenu'><a href='http://selox.mpi-cbg.de/cgi-bin/selox/workflow'>Workflow</a></span></div>"
print "<div style='margin-top:5px;width:100px;height:10px;background-color:#2682D5;text-align:center;padding:10px;'><span id='sidemenu'><a href='http://selox.mpi-cbg.de/cgi-bin/selox/about'>About</a></span></div>"
print "<div style='margin-top:5px;width:100px;height:10px;background-color:#2682D5;text-align:center;padding:10px;'><span id='sidemenu'><a href='http://selox.mpi-cbg.de/cgi-bin/selox/sampledata'>Sample Data</a></span></div>"
print "<div style='margin-top:5px;width:100px;height:10px;background-color:#2682D5;text-align:center;padding:10px;'><span id='sidemenu'><a href='http://selox.mpi-cbg.de/cgi-bin/selox/contact'>Contact</a></span></div>"
print "</div>"
print "</div>"

print "<div id='workflow'>"
print "<img src='http://selox.mpi-cbg.de/selox/workflow_web.jpg' />"
print "</div>"
print "</body>"
print "</html>"
