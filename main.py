from sys import argv
#from Bio import Fasta
from Bio import SeqIO
from search import makePatternHashList
from score import getLTRs, scoreLtrs
from elchange import cleanEndings
import os
from pickle import dump
