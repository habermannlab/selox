#!/usr/bin/python2
def cleanEndings(fileName):
    file = open(fileName)
    text = file.read()
    file.close()
    file = open(fileName, "w")
    file.write(text.replace("\r", "\n"))
    file.close()
    return
